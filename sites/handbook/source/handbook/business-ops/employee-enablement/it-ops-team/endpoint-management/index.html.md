---
layout: handbook-page-toc
title: "Endpoint Management at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Endpoint management overview

## What is an endpoint?

An endpoint is any device that is physically an endpoint on a network. These can include laptops, desktops, mobile phones, tablets, servers, and virtual environments. For the purposes of this current project however, the scope is limited to Apple laptops.

## What is endpoint management?

End-point management is used to protect the corporate network when accessed via remote devices such as laptops. Each laptop with a remote connection to the network creates a potential entry point for security threats.

# Endpoint management at GitLab

At GitLab, we plan to use centralized laptop management for company-issued laptops. If we start doing that, we'll change this sentence. This page is live in the handbook so we can respond to feedback.

At this stage, if you are in possession of a company-issued Apple laptop, the details below apply to you. Non Apple laptops, personal laptops or mobile devices are not in scope of this iteration.

## Expectation and success criteria

Our expectation is that we will find 10% of our Macbook devices with no harddrive encryption and 5% of the operating systems are not at the current patch level.

If the number of encrypted drives is below 2% and the number of out of date OS is below 1% we will re-consider making end-point management required for all Mac OS users.

## Why is this necessary?

In order to achieve compliance with frameworks such as SOX (required as part of public company readiness), [SOC](https://www.aicpa.org/content/dam/aicpa/interestareas/frc/assuranceadvisoryservices/downloadabledocuments/trust-services-criteria.pdf), and in preparation of [FedRAMP](https://www.fedramp.gov/assets/resources/documents/FedRAMP_Security_Assessment_Framework.pdf) and [ISO 27001](https://www.isms.online/iso-27001/annex-a-8-asset-management/), certain protections of company assets are mandated.

Given that transparency is so ingrained in our culture, the risk of any laptop having confidential or PII data is high (e.g. Slack contains team-member phone numbers).

Additionally, to meet the rigorous security requirements of enterprise customers who desire to use our service, an endpoint management solution is necessary. We have to select an endpoint management solution that will accomplish the following:

1. Allow for software to be remotely deployed without requiring manual installation
1. Maintain asset inventory of all GitLab owned devices
1. Software license management
1. Enable confirmation that whole disk encryption has been enabled (using the Mac OS built-in FileVault feature)
1. Provide the ability to remotely wipe a device that has been lost or stolen
1. Allow for the configuration of security features such as required passwords and OS updates

### What is not necessary?

What the endpoint management solution **does not** do:

1. Content filtering
1. Collect, log or track personal activity (including website visits or purchases)
1. Remote viewing
1. Key-logging

# Apple laptops

We performed a proof of concept of multiple solutions and determined [JAMF](https://www.jamf.com/) to be the best option due to its complete suite of features that meets GitLab compliance and customer requirements as well as providing end-user transparency through accessible logs.

## What is JAMF?

JAMF is an Apple device management solution used by system administrators to configure and automate IT administration tasks for macOS, iOS, and tvOS devices. The current project will focus solely on macOS devices

## Why JAMF?


JAMF was selected as a best option that covered our list of requirements:

1. Lightweight agent with minimal compute footprint
1. Ability to lock & wipe the laptop remotely
1. Security management policies - Disk encryption, password strength
1. Schedule OS & software installation and updates
1. Receive non-compliant endpoint notifications
1. SSO support
1. Hardware health status (battery and harddrive health)
1. Report serial number, model/type. enumerate hardware and OS version
1. Enumerate installed software (+browser extensions)
1. Software management
1. Log data on user logins, IP address and machine info
1. Allow users to see what's being collected

## What data is JAMF collecting? How can I view it?

The data collected from your company-issued Apple laptop can be viewed in XML format by accessing `~/Documents/Jamf_Data.xml` on your Apple laptop.
We recommend an XML parser to view the data.

## Implementation plan

The original implementation plan was:

* 1st wave (2020-06-15): IT, Security
* 2nd wave (2020-06-22): Cross Functional Pilot Groups
* 3rd wave (2020-06-29): E-Group
* 4th wave (2020-07-06): People, Recruiting, Finance, Product, Marketing
* 5th wave (2020-07-13): Sales, Legal, Chief of Staff team
* 6th wave (2020-07-20): Engineering

# Linux laptops

We do not have Linux-based endpoint management in place yet. There will be a second initiative to address Linux management later in FY21.

# Windows laptops

The Windows operating system is not a supported platform at GitLab, as described in the [Internal Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/#unable-to-use-company-laptop). If you’re using a Windows laptop, please contact [IT](/handbook/business-ops/it-help/#get-in-touch) to have a company laptop shipped to you.

# Support Information

Please review the [Frequently Asked Questions](#frequently-asked-questions) before asking for additional help.

Slack: [#it-help](https://gitlab.slack.com/archives/CK4EQH50E)

# Frequently Asked Questions

Please note, this section is continuously being updated with answers to common questions that GitLab team members have. This section is expected to grow significantly.

## Problem description

### Is endpoint management necessary?

Yes. Centralized endpoint management is common and necessary in enterprise organizations looking to achieve large scale growth, going public, and certifications. This is an expectation of our customers to meet their standards in order to utilize our service.

### Why are we using a third party endpoint management system?

The Jamf Pro endpoint management solution provides a lot of advantages over an open-source/build-it-yourself solution. Some of these include integration with our Single Sign-on Identity management system (Okta), Security and access profiles, and a self-service application that allows users to easily install officially supported applications. While a read-only solution would address some of these basic tenets, not everyone in the company is technical enough or motivated to manage the security of their machine. Therefore we require a solution that can be an active component in enforcing security measures.

Gitlab is a fully remote/SaaS first company, so the backend Jamf server will not be self-hosted. We will be using their SaaS service for hosting.

## Safeguards and controls

### Who owns and manages Jamf at GitLab?

GitLab IT Operations is the owner of Jamf and the [Manager, IT](/job-families/finance/manager-it/) is the DRI.

### Who ensures IT Operations is managing the tool correctly and ethically?

As with any enterprise tool, both the Security and Legal team will perform audits to ensure that Admins have the correct least access privilege and are adhering to our code of conduct when using the tool Admins that abuse the endpoint monitoring tools face disciplinary action, up to dismissal, civil/criminal prosecution, and damage claims.

### What will be the change, review, and socialization process for configuration changes to Jamf?

It will be no different than our current process for change management which is outlined here: https://about.gitlab.com/handbook/business-ops/business-technology-change-management/

## Endpoint management access

### Is my personal activity being monitored?

No. This is not an activity monitoring solution.

### Does this mean that you're able to view my browsing history?

No, browsing activity will neither be tracked nor monitored.

### Will remote viewing occur?

No, per policy we will not perform screen sharing. If laptop support is needed, it will be upon request with your desktop shared through Zoom.

### Can someone Secure Shell (SSH) into my laptop?

Only the IT Team will have administrative access into Jamf, and interactive Secure Shell into user's laptops will not be done without first obtaining permission from the user.

### Who has access to the data that's being collected? Who can manage security policies? Who can trigger remote laptop wipes?

The IT Operations team has access to this data and has these permissions.
Any of the IT team can trigger a remote wipe in cases where a laptop is lost or stolen, or a team-member is off-boarded. Policy creation and management will be limited to a small group within IT Operations (currently only 3 people). We will not put a technical safeguard in place to prevent remote laptop wipes by a single IT operations team-member, this isn't practical. Only a few people will have this ability. If they use a wipe maliciously we will consider filing a police report and we might start a criminal prosecution. To prevent an ITOps team-member from doing this after getting offboarded we remove their access immediately in the case of an involuntary termination as per our offboarding policy.

### How much notice will be provided before a change is made to the data collection and operations of Jamf?

While we don't expect to be making any changes to our currently defined data privacy policy, should the need arise due to a request from the Security or Legal departments, that change would go through the same change management process as defined above.

### Where can I view data collected from my laptop?

As outlined in the merge request, all data being collected by the Jamf agent will be listed in an XML file in each user's home directory located here ~/Documents/Jamf_Data.xml. Jamf also offers wide community support, and customizability and we fully expect to take advantage of this and iterate towards more transparency. In the meantime ITOps is happy to hop on a call with any team-member and show them how Jamf works and what data has been collected from their machine.


### Will a user be notified that the endpoint management software is installing something? And will the user know what has been installed?

In general, all changes performed by Jamf will notify the user ahead of time and offer the user the option to defer the change in cases where the timing is inconvenient to the user. However, that deferral is limited and the user will eventually be forced to apply the update in cases where the update is related to security.

### What about the risk of Jamf being used as an attack vector against business or personal interests?

Jamf, including the SaaS component, has passed our usual security procedures for suppliers, and we're philosophical about this possibility - although the potential hazards are high, we judge the risks to be low enough that this won't stop us from continuing with the current proposal. For business interests, this is our call to make, although you can [disagree, commit, and disagree](/handbook/values/#disagree-commit-and-disagree).

Personal interests are more difficult, especially given GitLab's status as a remote-only company - individuals may differ in their evaluation of what risks are acceptable here, and it is not our call to make. If this describes you, then your best option is to practice stricter separation of personal and business interests to avoid the conflict.

For instance, you could:

* Avoid using the endpoint for personal tasks - *if you are concerned about a remote wipe causing personal data loss on the endpoint*
* Isolate the endpoint to its own virtual or physical network - *if you are concerned about a compromise making other endpoints on your network vulnerable*
* Isolate the endpoint in rented office premises - *if you are concerned about a compromise of the camera or microphone*

Remember that you can [spend company money](/handbook/spending-company-money/) like it's your own to get a working environment that is suitable for you.

## Eligibility

### Are personal laptops in scope?

Personal laptops are not in scope here since they are not issued by GitLab. If you are using a personal laptop for business purposes please ensure you comply with our [Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/#bring-your-own-device-byod) at all times.


## Compliance frameworks

### How does this align with GDPR (EU General Data Protection Regulation)?

Endpoint management is a practice that is aligned with GDPR when done right. Jamf is GDPR compliant, you can read more about it [here](https://www.jamf.com/jamf-nation/articles/520/complying-with-gdpr-requests-in-jamf-pro) and [here](https://www.jamf.com/blog/what-you-need-to-know-about-gdpr/).
